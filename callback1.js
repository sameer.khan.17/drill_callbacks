const fs = require('fs');
function getBoardInfo(filePath, boardId, callback) {
    if (filePath == undefined) {
        callback(new Error('filePath is not passed'));
    } else if (boardId == undefined) {
        callback(new Error('No Board Id is passed'));
    } else if (callback == undefined) {
        console.error('There is an error:');
        console.error(new Error('Callback function is not passed'));
    } else {
        setTimeout(() => {
            fs.readFile(filePath, function (err, data) {
                if (err) {
                    callback(err);
                } else {
                    try {
                        const boards = JSON.parse(data);
                        const boardInfo = boards.filter((boardData) => {
                            return boardData.id == boardId;
                        });
                        if (boardInfo.length > 0) {
                            callback(null, boardInfo);
                        }
                        else {
                            callback(new Error('Board Id not found'));
                        }
                    } catch (error) {
                        callback(error);
                    }
                }
            });
        }, 2 * 1000);
    }
}
module.exports = getBoardInfo;