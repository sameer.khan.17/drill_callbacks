const getBoardInfo = require('./callback1.js');
const getLists = require('./callback2.js');
const getCards = require('./callback3.js');

function getInfo() {
    setTimeout(() => {
        getBoardInfo('./boards.json', 'mcu453ed', function (err, boardInfo) {
            if (err) {
                console.error('There is an error');
                console.error(err);
            } else {

                console.log('Information for Thanos board');
                console.log(boardInfo);

                getLists('./lists.json', 'mcu453ed', function (err, lists) {
                    if (err) {
                        console.error('There is an error');
                        console.error(err);
                    } else {

                        console.log('Lists for Thanos board');
                        console.log(lists);

                        listInfoMind = lists.find(listItem => {
                            return listItem.name == "Mind";
                        });
                        listInfoSpace = lists.find(listItem => {
                            return listItem.name == "Space";
                        });

                        getCards('./cards.json', [listInfoMind.id, listInfoSpace.id], function (err, cards) {
                            if (err) {
                                console.error('There is an error');
                                console.error(err);
                            } else {

                                console.log('Cards for Mind and Space list');
                                console.log(cards);
                            }
                        });
                    }
                });
            }
        });
    }, 2 * 1000);
}
module.exports = getInfo;