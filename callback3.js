const fs = require('fs');
function getCards(filePath, listId, callback) {
    if (filePath == undefined) {
        callback(new Error('File Path is not passed'));
    } else if (listId == undefined || listId.length == 0) {
        callback(new Error('No List Id is passed'));
    } else if (callback == undefined) {
        console.error('There is an error');
        console.error(new Error('Callback function is not passed'));
    } else {
        setTimeout(() => {
            fs.readFile(filePath, function (err, data) {
                if (err) {
                    callback(err);
                } else {
                    try {
                        const cards = JSON.parse(data);

                        let requiredCards = [];

                        listId.forEach(id => {
                            if (cards.hasOwnProperty(id)) {
                                requiredCards.push(cards[id]);
                            }
                        });
                        if (requiredCards.length > 0) {
                            callback(null, requiredCards);
                        } else {
                            callback(new Error('ListId not found'));
                        }
                    } catch (error) {
                        callback(error);
                    }
                }
            });
        }, 2 * 1000);
    }
}
module.exports = getCards;