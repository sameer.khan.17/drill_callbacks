const getBoardInfo = require('./callback1.js');
const getLists = require('./callback2.js');
const getCards = require('./callback3.js');
const fs = require('fs');

function getInfo() {
    setTimeout(() => {
        getBoardInfo('./boards.json', 'mcu453ed', function (err, boardInfo) {
            if (err) {
                console.error('There is an error');
                console.error(err);
            } else {

                console.log('Information for Thanos board');
                console.log(boardInfo);

                getLists('./lists.json', 'mcu453ed', function (err, lists) {
                    if (err) {
                        console.error('There is an error');
                        console.error(err);
                    } else {

                        console.log('Lists for Thanos board');
                        console.log(lists);

                        fs.readFile('./cards.json', function (err, data) {
                            if (err) {
                                console.error('There is an error');
                                console.error(err);
                            } else {

                                try {
                                    const cards = JSON.parse(data);

                                    let listIds = [];

                                    for (const listId in cards) {
                                        listIds.push(listId);
                                    }
                                    getCards('./cards.json', listIds, function (err, cards) {
                                        if (err) {
                                            console.error('There is an error');
                                            console.error(err);
                                        } else {

                                            console.log('Cards for all the lists');
                                            console.log(cards);
                                        }
                                    });
                                } catch (error) {
                                    console.error('There is an error');
                                    console.error(error);
                                }
                            }
                        });
                    }
                });
            }
        });
    }, 2 * 1000);
}
module.exports = getInfo;