const getBoardInfo = require('../callback1.js');

//Testing when everything is correctly passed
getBoardInfo('./boards.json', 'abc122dc', function (err, boardInfo) {
    if (err) {
        console.error('There is an error');
        console.error(err);
    } else {
        console.log('Board Information:');
        console.log(boardInfo);
    }
});

//Testing when wrong boardId is passed
getBoardInfo('./boards.json', 'mcu453ed123', function (err, boardInfo) {
    if (err) {
        console.error('There is an error');
        console.error(err);
    } else {
        console.log('Board Information:');
        console.log(boardInfo);
    }
});

//Testing when wrong filepath is is passed
getBoardInfo('./boards111.json', 'mcu453ed', function (err, boardInfo) {
    if (err) {
        console.error('There is an error');
        console.error(err);
    } else {
        console.log('Board Information:');
        console.log(boardInfo);
    }
});

//Testing when there is a bad json file
getBoardInfo('./boardsCorrupt.json', 'mcu453ed', function (err, boardInfo) {
    if (err) {
        console.error('There is an error');
        console.error(err);
    } else {
        console.log('Board Information:');
        console.log(boardInfo);
    }
});

//Testing when callback function is not passed
getBoardInfo('./boards.json', 'mcu453ed');