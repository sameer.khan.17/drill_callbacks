const getLists = require('../callback2.js');

//Testing when everything is correctly passed
getLists('./lists.json', 'abc122dc', function (err, lists) {
    if (err) {
        console.error('There is an error');
        console.error(err);
    } else {
        console.log('Lists for the passed Board Id');
        console.log(lists);
    }
});

//Testing when wrong boardId is passed
getLists('./lists.json', 'mcu453ed1234', function (err, lists) {
    if (err) {
        console.error('There is an error');
        console.error(err);
    } else {
        console.log('Lists for the passed Board Id');
        console.log(lists);
    }
});

//Testing when wrong filepath is passed
getLists('./lists123.json', 'mcu453ed', function (err, lists) {
    if (err) {
        console.error('There is an error');
        console.error(err);
    } else {
        console.log('Lists for the passed Board Id');
        console.log(lists);
    }
});

//Testing when there is a bad json file
getLists('./listsCorrupt.json', 'mcu453ed', function (err, lists) {
    if (err) {
        console.error('There is an error');
        console.error(err);
    } else {
        console.log('Lists for the passed Board Id');
        console.log(lists);
    }
});

//Testing when callback function is not passed
getLists('./lists.json', 'mcu453ed');