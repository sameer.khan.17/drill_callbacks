const getCards = require('../callback3.js');

//Testing when everything is correctly passed
getCards('./cards.json', ['qwsa221'], function (err, cards) {
    if (err) {
        console.error('There is an error');
        console.error(err);
    } else {
        console.log('Cards for the passed list id');
        console.log(cards);
    }
});

//Testing when wrong listId is passed
getCards('./cards.json', ['qwsa221890'], function (err, cards) {
    if (err) {
        console.error('There is an error');
        console.error(err);
    } else {
        console.log('Cards for the passed list id');
        console.log(cards);
    }
});

//Testing when wrong filepath is passed
getCards('./cards123.json', ['qwsa221'], function (err, cards) {
    if (err) {
        console.error('There is an error');
        console.error(err);
    } else {
        console.log('Cards for the passed list id');
        console.log(cards);
    }
});

//Testing when there is a bad json file
getCards('./cardsCorrupt.json', ['qwsa221'], function (err, cards) {
    if (err) {
        console.error('There is an error');
        console.error(err);
    } else {
        console.log('Cards for the passed list id');
        console.log(cards);
    }
});

//Testing when callback function is not passed
getCards('./cards.json', ['qwsa221']);