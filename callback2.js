const fs = require('fs');
function getLists(filePath, boardId, callback) {
    if (filePath == undefined) {
        callback(new Error('filePath is not passed'));
    } else if (boardId == undefined) {
        callback(new Error('No Board Id is passed'));
    } else if (callback == undefined) {
        console.error('There is an error');
        console.error(new Error('Callback function is not passed'));
    } else {
        setTimeout(() => {
            fs.readFile(filePath, function (err, data) {
                if (err) {
                    callback(err);
                } else {
                    try {
                        const lists = JSON.parse(data);
                        if (lists.hasOwnProperty(boardId)) {
                            callback(null, lists[boardId]);
                        } else {
                            callback(new Error('Board id not found'));
                        }
                    } catch (error) {
                        callback(error);
                    }
                }
            });
        }, 2 * 1000);
    }
}
module.exports = getLists;